# Projet Open-9Sat

( Open Nano (-9) Sat)

# Bienvenue !

Le but de ce projet est de promouvoir l'Open Source dans le domaine des cubesats et de développer un cubesat fonctionnel.

## Pourquoi Open Source ?

Pour pouvoir partager un développement commun autour d'une platforme Open Source.
Pour pouvoir challenger les pièces sur étagère développer par les différentes entreprises dédié au CubeSat.
Créer et partager une banque de donnée d'information technique autour des CubeSat.
Surement plein d'autres choses cool ... !

## Dans quelle langue ?

En Français en premier lieu sinon en Anglais.

# Le contenu :

- une platforme Hardware compléte : Mécanique, Télécomunication, Puissance, Electronique, Attitude, Ordinateur de bord,...
- Un Software complet : Logiciel de Bord, Logiciel d'attitude, ...
- Autre : Orbitographie, Intégration, Test, ...

## Le but :

- Aider au développement de CubeSat
- Apprendre !
- Partager !!

## La phylosophie :

- Pas de prise de tête
- Ne pas reinventer la roue
- Partager et apprendre ensemble

## La Licence :

GNU GPL version 3 Pour le Software
Creative Commons version 4 Pour le Hardware et les documents


## Contact :


